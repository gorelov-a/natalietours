package com.getjavajob.training.web03.gorelov.dao;

import com.getjavajob.training.web03.gorelov.domain.Day;

import java.util.Date;
import java.util.List;

/**
 * Created by Антон on 04.07.2015.
 */
public interface DayDao {
    Day saveOrUpdate(Day day);

    Day getById(long id);

    void removeByDate(Date date);

    List<Day> getDays(Date statDate, Date endDate);

    void delete(Day day);
}
