package com.getjavajob.training.web03.gorelov.web;

import com.getjavajob.training.web03.gorelov.domain.Day;
import com.getjavajob.training.web03.gorelov.service.DayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by Anton on 09.07.2015.
 */
@Controller
public class DayController {
    @Autowired
    private DayService service;

    private void addToCalendar(String add) throws ParseException {
        String[] addDays = add.split(",");
        DateFormat dateFormat = DateFormat.getDateInstance();
        List<Day> addDayList = new ArrayList<>();
        for (String s : addDays) {
            if (!"".equals(s)) {
                Date date = dateFormat.parse(s);
                Day day = new Day();
                day.setDate(date);
                addDayList.add(day);
            }
        }
        service.saveDays(addDayList);
    }

    private void deleteFromCalendar(String remove) throws ParseException {
        DateFormat dateFormat = DateFormat.getDateInstance();
        String[] delDays = remove.split(",");
        List<Day> delDayList = new ArrayList<>();
        for (String s : delDays) {
            if (!"".equals(s)) {
                Date date = dateFormat.parse(s);
                Day day = new Day();
                day.setDate(date);
                delDayList.add(day);
            }
        }
        service.removeDays(delDayList);
    }

    private void addMarkedDays(Model model, int count, Calendar calendar) {
        boolean first = true;
        for (int i = 1; i <= count; ++i) {
            if (first) {
                first = false;
            } else {
                calendar.roll(Calendar.MONTH, 1);
            }
            Day day = new Day();
            day.setDate(calendar.getTime());
            List<String> markDays = service.getDays(day);
            model.addAttribute("markDays" + i, markDays);
        }
    }

    @RequestMapping("/getCalendar")
    public String getCalendar(@RequestParam(value = "startDate", required = true) String dateString, Model model) throws ParseException {
        SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
        Date date = format1.parse(dateString);
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        calendar.set(Calendar.DATE, 1);
        model.addAttribute("month", calendar.get(Calendar.MONTH));
        model.addAttribute("year", calendar.get(Calendar.YEAR));
        addMarkedDays(model, 3, calendar);
        return "calendar";
    }

    @RequestMapping("/updateCalendar")
    public String updateCalendar(@RequestParam(value = "addDays", required = true) String add, @RequestParam(value = "removeDays", required = true) String remove) throws ParseException {
        addToCalendar(add);
        deleteFromCalendar(remove);
        return "index";
    }
}
