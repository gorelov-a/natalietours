package com.getjavajob.training.web03.gorelov.domain;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Антон on 04.07.2015.
 */
@NamedQueries({
        @NamedQuery(name = "getDays", query = "select d from Day d where d.date >= :startDate and d.date < :endDate"),
        @NamedQuery(name = "remove", query = "delete  from Day d where d.date = :date")
})

@Entity
@Table(name = "DAYS")
public class Day {
    @Id
    @Column(name = "DAY_ID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Temporal(TemporalType.DATE)
    @Column(name = "DATE", unique = true)
    private Date date;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
