package com.getjavajob.training.web03.gorelov.service;

import com.getjavajob.training.web03.gorelov.domain.Day;

import java.util.List;

/**
 * Created by ����� on 09.07.2015.
 */
public interface DayService {
    List<String> getDays(Day day);

    void saveDays(List<Day> days);

    void removeDays(List<Day> days);
}
