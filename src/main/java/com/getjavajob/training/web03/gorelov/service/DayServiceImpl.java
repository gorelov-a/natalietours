package com.getjavajob.training.web03.gorelov.service;

import com.getjavajob.training.web03.gorelov.dao.DayDao;
import com.getjavajob.training.web03.gorelov.domain.Day;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

/**
 * Created by Anton on 09.07.2015.
 */
@Service
@Transactional
public class DayServiceImpl implements DayService {
    @Autowired
    DayDao dayDao;

    @Override
    public List<String> getDays(Day startDay) {
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(startDay.getDate());
        calendar.roll(Calendar.DATE, 1);
        calendar.roll(Calendar.MONTH, 1);
        calendar.roll(Calendar.DATE, -1);
        List<Day> days = dayDao.getDays(startDay.getDate(), calendar.getTime());
        List<String> dateStrings = new ArrayList<>();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd");
        for (Day myDay : days) {
            dateStrings.add(dateFormat.format(myDay.getDate()));
        }
        return dateStrings;
    }

    @Override
    public void saveDays(List<Day> days) {
        for (Day day : days) {
            dayDao.saveOrUpdate(day);
        }
    }

    @Override
    public void removeDays(List<Day> days) {
        for (Day day : days) {
            dayDao.removeByDate(day.getDate());
        }
    }
}
