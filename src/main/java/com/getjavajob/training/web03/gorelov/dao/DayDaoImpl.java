package com.getjavajob.training.web03.gorelov.dao;

import com.getjavajob.training.web03.gorelov.domain.Day;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Date;
import java.util.List;

/**
 * Created by Антон on 04.07.2015.
 */


@Component
public class DayDaoImpl implements DayDao {
    @PersistenceContext
    private EntityManager em;

    @Override
    public Day saveOrUpdate(Day day) {
        return em.merge(day);
    }

    @Override
    public Day getById(long id) {
        Day day = new Day();
        day.setId(id);
        return em.find(Day.class, day.getId());
    }

    @Override
    public void removeByDate(Date date){
        em.createNamedQuery("remove").setParameter("date",date).executeUpdate();
    }

    @Override
    public List<Day> getDays(Date statDate, Date endDate) {
        return em.createNamedQuery("getDays", Day.class).setParameter("startDate", statDate).setParameter("endDate", endDate).getResultList();
    }

    @Override
    public void delete(Day day) {
        em.remove(day);
    }
}
