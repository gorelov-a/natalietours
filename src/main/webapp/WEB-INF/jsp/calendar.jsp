<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>Bank Day</title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/bankday.css"/>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script charset="utf-8" src="${pageContext.request.contextPath}/js/bankday.js"></script>
    <script>
        var markDays1 = [];
        <c:forEach var="date" items="${markDays1}">
        markDays1.push(<c:out value="${date}"/>);
        </c:forEach>

        var markDays2 = [];
        <c:forEach var="date" items="${markDays2}">
        markDays2.push(<c:out value="${date}"/>);
        </c:forEach>

        var markDays3 = [];
        <c:forEach var="date" items="${markDays3}">
        markDays3.push(<c:out value="${date}"/>);
        </c:forEach>

        var arrayDaysAdd = [];
        var arrayDaysDel = [];

        $(document).ready(function () {
            getCalendar(${month}, ${year}, "calendar1", markDays1);
            getCalendar(${month}+1, ${year}, "calendar2", markDays2);
            getCalendar(${month}+2, ${year}, "calendar3", markDays3);

            $("td").click(function () {
                if (($(this).attr('class') == "bankday") || ($(this).attr('class') == "nodel")) {
                    var index = arrayDaysDel.indexOf($(this).attr('id'));
                    if (index != -1) {
                        arrayDaysDel[index] = "";
                        $(this).attr('class', "bankday");
                    } else {
                        arrayDaysDel.push($(this).attr('id'));
                        $(this).attr('class', "nodel");
                    }
                } else {
                    index = arrayDaysAdd.indexOf($(this).attr('id'));
                    if (index != -1) {
                        arrayDaysAdd[index] = "";
                        $(this).attr('class', "");
                    } else {
                        arrayDaysAdd.push($(this).attr('id'));
                        $(this).attr('class', "red");
                    }
                }
            });

            $("#updateForm").submit(function (eventObj) {
                $(this).append('<input type="hidden" name="addDays" value=\'' + arrayDaysAdd + '\'/>');
                $(this).append('<input type="hidden" name="removeDays" value=\'' + arrayDaysDel + '\'/>');
                return true;
            });
        });
    </script>
</head>
<body>
<h1 align="center">Банковский день</h1>

<form name="dateForm" action="${pageContext.request.contextPath}/getCalendar" method="post">
    <label style="margin-left: 50px">Дата:</label><input type="date" name="startDate" title="date">
    <input type="submit" value="Отправить">
</form>
<table id="calendar1" class="mytable">
    <thead>
    <tr>
        <td colspan="4"></td>
        <td colspan="3"></td>
    </tr>
    <tr>
        <td>Пн</td>
        <td>Вт</td>
        <td>Ср</td>
        <td>Чт</td>
        <td>Пт</td>
        <td>Сб</td>
        <td>Вс</td>
    </tr>
    </thead>
    <tbody></tbody>
</table>
<table id="calendar2" class="mytable">
    <thead>
    <tr>
        <td colspan="4"></td>
        <td colspan="3"></td>
    </tr>
    <tr>
        <td>Пн</td>
        <td>Вт</td>
        <td>Ср</td>
        <td>Чт</td>
        <td>Пт</td>
        <td>Сб</td>
        <td>Вс</td>
    </tr>
    </thead>
    <tbody></tbody>
</table>
<table id="calendar3" class="mytable">
    <thead>
    <tr>
        <td colspan="4"></td>
        <td colspan="3"></td>
    </tr>
    <tr>
        <td>Пн</td>
        <td>Вт</td>
        <td>Ср</td>
        <td>Чт</td>
        <td>Пт</td>
        <td>Сб</td>
        <td>Вс</td>
    </tr>
    </thead>
    <tbody></tbody>
</table>

<form id="updateForm" action="${pageContext.request.contextPath}/updateCalendar" method="post">
    <input type="submit" value="Отправить">
</form>
</body>
</html>
