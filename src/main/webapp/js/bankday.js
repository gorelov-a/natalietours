
function getCalendar(month, year, id, markDays) {
    var date = new Date();
    date.setDate(1);
    date.setMonth(month);
    date.setYear(year);
    var lastDayMonth = new Date(date.getFullYear(), date.getMonth() + 1, 0).getDate(), // последний день месяца
        weekdayEndMonth = new Date(date.getFullYear(), date.getMonth(), lastDayMonth).getDay(), // день недели последнего дня месяца
        weekdayStartMonth = new Date(date.getFullYear(), date.getMonth(), 1).getDay(), // день недели первого дня месяца
        calendar = '<tr>',
        months = ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"]; // название месяца, вместо цифр 0-11

    // пустые клетки до первого дня текущего месяца
    if (weekdayStartMonth != 0) {
        for (var i = 1; i < weekdayStartMonth; i++) calendar += '<td>';
    } else { // если первый день месяца выпадает на воскресенье, то требуется 7 пустых клеток
        for (var i = 0; i < 6; i++) calendar += '<td>';
    }
    // дни месяца
    for (var i = 1; i <= lastDayMonth; i++) {
        var isMarkedDay = false;
        for (var j = 0; j < markDays.length; ++j) {
            if (markDays[j] == i) {
                isMarkedDay = true;
                break;
            }
        }
        if (!isMarkedDay) {
            calendar += '<td id =' + i + "." + (date.getMonth() + 1) + "." + date.getFullYear() + '\">' + i;
        } else {
            calendar += '<td class="bankday" id =' + i + "." + (date.getMonth() + 1) + "." + date.getFullYear() + '\">' + i;  // сегодняшней дате можно задать стиль CSS
        }
        if (new Date(date.getFullYear(), date.getMonth(), i).getDay() == 0) {  // если день выпадает на воскресенье, то перевод строки
            calendar += '<tr>';
        }
    }

    // пустые клетки после последнего дня месяца
    if (weekdayEndMonth != 0) {
        for (var i = weekdayEndMonth; i < 7; i++) calendar += '<td>';
    }

    document.querySelector('#' + id + ' tbody').innerHTML = calendar;
    document.querySelector('#' + id + ' thead td:last-child').innerHTML = date.getFullYear();
    document.querySelector('#' + id + ' thead td:first-child').innerHTML = months[date.getMonth()];
}