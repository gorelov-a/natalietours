package com.getjavajob.training.web03.gorelov.service;

import com.getjavajob.training.web03.gorelov.domain.Day;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.transaction.Transactional;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by Anton on 09.07.2015.
 */
@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"/application-Context-Test.xml"})
public class DayServiceTest {
    @Autowired
    DayService service;
    private DateFormat dateFormat = DateFormat.getDateInstance();

    @Test
    public void testSaveDays() throws Exception {
        List<Day> days = new ArrayList<>();
        Day day = new Day();
        day.setDate(dateFormat.parse("01.01.2014"));
        days.add(day);
        day = new Day();
        day.setDate(dateFormat.parse("25.01.2014"));
        days.add(day);
        service.saveDays(days);

        day = new Day();
        day.setDate(dateFormat.parse("01.01.2014"));
        List<String> markDays = service.getDays(day);
        assertNotNull(markDays);
        assertEquals(markDays.size(), 2);
    }

    @Test
    public void testRemoveDays() throws Exception {
        List<Day> days = new ArrayList<>();
        Day day = new Day();
        day.setDate(dateFormat.parse("01.01.2014"));
        days.add(day);
        day = new Day();
        day.setDate(dateFormat.parse("25.01.2014"));
        days.add(day);
        service.saveDays(days);

        day = new Day();
        day.setDate(dateFormat.parse("01.01.2014"));
        List<String> markDays = service.getDays(day);
        assertNotNull(markDays);
        assertEquals(markDays.size(), 2);
        service.removeDays(days);
        day = new Day();
        day.setDate(dateFormat.parse("01.01.2014"));
        markDays = service.getDays(day);
        assertNotNull(markDays);
        assertEquals(markDays.size(), 0);
    }
}