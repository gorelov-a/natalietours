package com.getjavajob.training.web03.gorelov.dao;

import com.getjavajob.training.web03.gorelov.domain.Day;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.transaction.Transactional;
import java.text.DateFormat;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by ����� on 06.07.2015.
 */
@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"/application-Context-Test.xml"})
public class DayDaoTest {
    @Autowired
    DayDao dayDao;
    private DateFormat dateFormat = DateFormat.getDateInstance();

    @Test
    public void testSaveOrUpdate() throws Exception {
        Day day = new Day();
        day.setDate(dateFormat.parse("06.07.2015"));
        dayDao.saveOrUpdate(day);
        day.setDate(dateFormat.parse("05.07.2015"));
        dayDao.saveOrUpdate(day);
        List list = dayDao.getDays(dateFormat.parse("05.07.2015"), dateFormat.parse("7.07.2015"));
        assertNotNull(list);
        assertEquals(list.size(), 2);
    }

    @Test
    public void testGetById() throws Exception {
        Day day = dayDao.getById(0);
        assertNotNull(day);
        assertEquals(day.getId(), 0);
        assertEquals(dateFormat.parse("09.07.2015"), day.getDate());
    }

    @Test
    public void testGetDays() throws Exception {
        List list = dayDao.getDays(dateFormat.parse("08.07.2015"), dateFormat.parse("12.07.2015"));
        assertNotNull(list);
        assertEquals(list.size(), 3);
    }

    @Test
    public void testDelete() throws Exception {
        Day day = dayDao.getById(0);
        dayDao.delete(day);
        day = dayDao.getById(0);
        assertNull(day);
    }

    @Test
    public void testRemoveByDate() throws Exception {
        Date date = dateFormat.parse("09.07.2015");
        dayDao.removeByDate(date);
        Day day = dayDao.getById(0);
        assertNull(day);
    }
}